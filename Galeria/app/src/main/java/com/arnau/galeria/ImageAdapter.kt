package com.arnau.galeria

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.support.v4.view.ViewPager



class ImageAdapter(val ctx: Context) : PagerAdapter()
{
    val inflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    val idsImatges = arrayOf(
        R.drawable.programador,
        R.drawable.programador1,
        R.drawable.lenguaje_programacion,
        R.drawable.programador2)

    override fun getCount(): Int {
        return idsImatges.size
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj as LinearLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        //super.instantiateItem(container, position)
        val view = inflater.inflate(R.layout.image_page, container, false)
        val imageView = view.findViewById<ImageView>(R.id.image_view)
        imageView.setImageResource(idsImatges[position])
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as LinearLayout)
    }
}