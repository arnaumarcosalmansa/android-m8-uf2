package com.arnau.myapplication

import android.content.Context
import android.media.AudioManager
import android.media.SoundPool
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.assignSoundToButtons()
    }

    private fun assignSoundToButtons() {
        val soundPool = SoundPool(16, AudioManager.STREAM_MUSIC, 100)
        val context = this.applicationContext

        val barkButton = findViewById<Button>(R.id.barkloud)
        barkButton.setOnClickListener(SoundPoolOnClickListener(context, soundPool, R.raw.barkloud))

        val catButton = findViewById<Button>(R.id.cat)
        catButton.setOnClickListener(SoundPoolOnClickListener(context, soundPool, R.raw.cat))

        val cowButton = findViewById<Button>(R.id.cow)
        cowButton.setOnClickListener(SoundPoolOnClickListener(context, soundPool, R.raw.cow))

        val explosionButton = findViewById<Button>(R.id.explosion)
        explosionButton.setOnClickListener(SoundPoolOnClickListener(context, soundPool, R.raw.explosion))

        val laserButton = findViewById<Button>(R.id.laser)
        laserButton.setOnClickListener(SoundPoolOnClickListener(context, soundPool, R.raw.laser))

        val pickupButton = findViewById<Button>(R.id.laser)
        pickupButton.setOnClickListener(SoundPoolOnClickListener(context, soundPool, R.raw.pickup))
    }
}

class SoundPoolOnClickListener(val context: Context, val soundPool: SoundPool, val audioResource: Int) : View.OnClickListener
{
    val rightvol = 1.0f
    val leftvol = 1.0f

    init
    {
        soundPool.load(context, audioResource, 1);
    }

    override fun onClick(v: View?)
    {
        soundPool.play(audioResource, leftvol, rightvol, 1, 0, 1.0f)
    }
}
