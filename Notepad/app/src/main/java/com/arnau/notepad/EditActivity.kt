package com.arnau.notepad

import android.content.DialogInterface
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.EditText
import android.widget.TextView
import java.io.File
import java.lang.IllegalArgumentException

class EditActivity : AppCompatActivity()
{
    var files: FileInterface? = null
    var filename: String? = null
    var text: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_activity)
        filename = intent.getStringExtra("file_name")
        val path: String = intent.getStringExtra("file_path")
        files = FileInterface(path)
        val content: String? = files?.readFile(filename!!)
        text = findViewById(R.id.text)
        text?.setText(content, TextView.BufferType.EDITABLE)

        this.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onBackPressed()
    {
        //super.onBackPressed()
        val builder: AlertDialog.Builder? = this?.let {
            AlertDialog.Builder(this)
        }
        //builder?.setView(R.layout.modal)
        builder?.setTitle("¿Guardar archivo?")
        builder?.setPositiveButton("Guardar", DialogInterface.OnClickListener { dialog, which ->
            val editText: EditText? = (dialog as AlertDialog).findViewById<EditText>(R.id.text)
            files?.writeFile(filename!!, text?.text.toString())
            super.onBackPressed()
        })
        builder?.setNeutralButton("Salir sin guardar", DialogInterface.OnClickListener { dialog, which ->
            super.onBackPressed()
        })
        builder?.setNegativeButton("Cancelar", DialogInterface.OnClickListener { dialog, which ->  })
        val dialog: AlertDialog? = builder?.create()
        dialog?.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return true
    }
}
