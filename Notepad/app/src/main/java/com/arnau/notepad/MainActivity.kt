package com.arnau.notepad

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.database.DataSetObserver
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.fileitem.*
import java.io.File
import java.lang.IllegalArgumentException
import java.nio.file.Path

class MainActivity : AppCompatActivity(){

    var filesPath: String? = null;
    var instance: AppCompatActivity? = null
    var files: FileInterface? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        instance = this
        filesPath = "Android/data/" + packageName + "/files/texts/"

        val mediaStatus: String = Environment.getExternalStorageState()
        if(Environment.MEDIA_MOUNTED.equals(mediaStatus))
        {
            val root: File = this.getExternalFilesDir(null)
            files = FileInterface(root.absolutePath)
            val filenames: Array<String>? = files?.listFiles()
            val listview: ListView = findViewById(R.id.listview)
            listview.adapter = FileListAdapter(this, R.layout.fileitem, R.id.filename, filenames!!)
            listview.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
                val intent: Intent = Intent(instance, EditActivity::class.java)
                val data: Bundle = Bundle()
                data.putString("file_name", filenames?.get(position))
                data.putString("file_path", root.absolutePath)
                intent.putExtras(data)
                startActivityForResult(intent, 1)
            }
        }
    }

    fun reloadFileList()
    {
        val listview: ListView = findViewById(R.id.listview)
        listview.adapter = FileListAdapter(this, R.layout.fileitem, R.id.filename, files?.listFiles()!!)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean
    {
        menuInflater.inflate(R.menu.activity_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean
    {
        when(item?.itemId){
            R.id.menu_add -> {addFileModal()}
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    {
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun addFileModal()
    {
        val builder: AlertDialog.Builder? = this?.let {
            AlertDialog.Builder(this)
        }
        builder?.setView(R.layout.modal)
        builder?.setTitle("Nombre del archivo")
        builder?.setPositiveButton("Aceptar", DialogInterface.OnClickListener { dialog, which ->
            val editText: EditText? = (dialog as AlertDialog).findViewById<EditText>(R.id.text)
            try
            {
                files?.createFile(editText?.text.toString())
                reloadFileList()
            }
            catch(e: IllegalArgumentException)
            {

            }
        })
        builder?.setNegativeButton("Cancelar", DialogInterface.OnClickListener { dialog, which ->  })
        val dialog: AlertDialog? = builder?.create()
        dialog?.show()
    }
}

class FileListAdapter constructor(ctx: Context, layout: Int, container: Int, data: Array<String>):
    ArrayAdapter<String>(ctx, layout, container, data)
{

}
