package com.arnau.gpstracker;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;

import java.util.Locale;

public class PreferencesActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        load();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPref.registerOnSharedPreferenceChangeListener(this);


    }

    private void load()
    {
        ActionBar bar = getSupportActionBar();
        bar.setTitle(R.string.preferencias);
        getSupportFragmentManager().beginTransaction().replace(android.R.id.content, new PreferencesFragment()).commitAllowingStateLoss();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        String langKey = "preference_language";
        String colorKey = "preference_color";
        if(key.equals(langKey))
        {
            String lang = sharedPreferences.getString(langKey, "es");
            setLocale(lang);
            load();
        }
        else if(key.equals(colorKey))
        {
            //String color = sharedPreferences.getString(colorKey, getResources().getString(R.string.color_ttb));
            //setColor(color);
            load();
        }
    }

    private void setLocale(String lang)
    {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}


