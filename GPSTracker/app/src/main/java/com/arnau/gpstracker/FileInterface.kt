package com.arnau.notepad

import java.io.*
import java.lang.IllegalArgumentException
import java.lang.StringBuilder

class FileInterface constructor(var pathToFiles: String)
{
    private val path: String = pathToFiles
    private val parentDirectory: File = File(path)

    init
    {
        parentDirectory.mkdirs()
    }

    public fun listFiles(): Array<String>
    {
        val fileNames: Array<String> = parentDirectory.list()
        return fileNames
    }

    public fun readFile(fileName: String): String
    {
        val file: File = File(parentDirectory.absolutePath + "/" + fileName)
        val reader: FileReader = FileReader(file)
        val text: String = reader.readText()
        return text
    }

    public fun writeFile(fileName: String, content: String)
    {
        val file: File = File(parentDirectory.absolutePath + "/" + fileName)
        val writer: FileWriter = FileWriter(file)
        writer.write(content)
        writer.flush()
    }

    public fun createFile(fileName: String)
    {
        if(fileName.contains("/|\\")) throw IllegalArgumentException()
        val file: File = File(parentDirectory.absolutePath + "/" + fileName)
        if(file.exists())  throw IllegalArgumentException()
        file.mkdirs()
        file.createNewFile()
    }
}