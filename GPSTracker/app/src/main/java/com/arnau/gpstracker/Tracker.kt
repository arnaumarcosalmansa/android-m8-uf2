package com.arnau.gpstracker

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.location.LocationProvider
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.preference.PreferenceManager
import android.util.Log
import java.io.File
import java.io.FileWriter
import java.lang.StringBuilder

class Tracker(val activity: Activity, val manager: LocationManager)
{
    var writer: FileWriter? = null
    val listener = object : LocationListener
    {
        val sb: StringBuilder = StringBuilder()

        public override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        public override fun onProviderEnabled(provider: String) {}
        public override fun onProviderDisabled(provider: String) {}
        public override fun onLocationChanged(location: Location)
        {
            val lon = location.longitude
            val lat = location.latitude
            val alt = location.altitude

            sb.append(lon).append(' ').append(lat).append(' ').append(alt).append('\n')
            val string = sb.toString()
            writer!!.write(string)
            writer!!.flush()
        }
    }
    public fun start(file: File){
    if (ContextCompat.checkSelfPermission(activity as Context, Manifest.permission.ACCESS_FINE_LOCATION)
        != PackageManager.PERMISSION_GRANTED && PackageManager.PERMISSION_GRANTED !=
        ContextCompat.checkSelfPermission(activity as Context, Manifest.permission.ACCESS_COARSE_LOCATION)
    ){
        throw UnsupportedOperationException()
    }

        val preferences = PreferenceManager.getDefaultSharedPreferences(activity as Context);
        val minTime = preferences.getString("preference_time", "").toLongOrNull() ?: 1
        val minDistance = preferences.getString("preference_distance", "").toFloatOrNull() ?: 1f
    writer = FileWriter(file)
    manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, listener)
}

    public fun stop()
    {
        manager.removeUpdates(listener)
        writer!!.close()
    }
}