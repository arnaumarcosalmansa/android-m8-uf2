package com.arnau.gpstracker

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import java.util.*

class PreferenceActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        load()
    }

    private fun load() {
        val bar = supportActionBar
        bar!!.setTitle(R.string.preferencias)
        supportFragmentManager.beginTransaction().replace(android.R.id.content, PreferencesFragment())
            .commitAllowingStateLoss()
    }
}