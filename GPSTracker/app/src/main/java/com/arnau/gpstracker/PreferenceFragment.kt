package com.arnau.gpstracker

import android.os.Bundle
import android.support.v7.preference.PreferenceFragmentCompat

class PreferenceFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.pref_all)
    }
}