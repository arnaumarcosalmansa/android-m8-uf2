package com.arnau.gpstracker

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.preference.PreferenceManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import android.widget.Toast
import com.arnau.notepad.FileInterface
import java.io.File
import java.lang.IllegalArgumentException

class MainActivity : AppCompatActivity() {

    var grabando = false;

    val COARSE_LOCATION_REQUEST = 80
    val FINE_LOCATION_REQUEST = 81

    var files: FileInterface? = null
    var locationManager: LocationManager? = null
    var tracker: Tracker? = null;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mediaStatus: String = Environment.getExternalStorageState()
        if(Environment.MEDIA_MOUNTED.equals(mediaStatus))
        {
            val root: File = this.getExternalFilesDir(null)
            val preferences = android.support.v7.preference.PreferenceManager.getDefaultSharedPreferences(this)
            val prefPath = preferences.getString("preference_path", "")
            val path = if(prefPath.equals("")) root.absolutePath else prefPath
            files = FileInterface(path)
        }
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        val started = startTracker()

        if(started) {
            val startButton = findViewById<Button>(R.id.start_button)
            startButton.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    addFileModal()
                }
            })

            val stopButton = findViewById<Button>(R.id.stop_button)
            stopButton.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    if(grabando)
                    {
                        tracker?.stop()
                        Toast.makeText(this@MainActivity, R.string.parar, Toast.LENGTH_LONG).show()
                    }
                    else
                    {
                        Toast.makeText(this@MainActivity, R.string.no_grabando, Toast.LENGTH_LONG).show()
                    }
                }
            })
        }
    }

    private fun startTracker(): Boolean
    {
        try
        {
            tracker = Tracker(this, locationManager!!)
            return true
        }
        catch(e: UnsupportedOperationException)
        {
            askForPermissions()
            return false
        }
    }

    private fun askForPermissions()
    {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION))
        {
            val alertBuilder = AlertDialog.Builder(this)
            alertBuilder.setTitle(R.string.permission_requested_title)
            alertBuilder.setMessage(R.string.permission_requested_message)
            alertBuilder.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                    COARSE_LOCATION_REQUEST
                )
            }).create().show()
        }
        else
        {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                COARSE_LOCATION_REQUEST
            )
        }

        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION))
        {
            val alertBuilder = AlertDialog.Builder(this)
            alertBuilder.setTitle(R.string.permission_requested_title)
            alertBuilder.setMessage(R.string.permission_requested_message)
            alertBuilder.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    FINE_LOCATION_REQUEST
                )
            }).create().show()
        }
        else
        {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                FINE_LOCATION_REQUEST
            )
        }
    }

    private fun startTrackerWithoutRequest(): Boolean
    {
        try
        {
            tracker = Tracker(this, locationManager!!)
            return true
        }
        catch(e: UnsupportedOperationException)
        {
            return false
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode)
        {
            COARSE_LOCATION_REQUEST -> {startTrackerWithoutRequest()}
            FINE_LOCATION_REQUEST -> {startTrackerWithoutRequest()}
        }
    }

    private fun addFileModal()
    {
        val builder: AlertDialog.Builder? = this?.let {
            AlertDialog.Builder(this)
        }
        builder?.setView(R.layout.modal)
        builder?.setTitle("Nombre del archivo")
        builder?.setPositiveButton("Aceptar", DialogInterface.OnClickListener { dialog, which ->
            if(grabando)
            {
                Toast.makeText(this, R.string.already_grabando, Toast.LENGTH_LONG).show()
                return@OnClickListener
            }
            val editText: EditText? = (dialog as AlertDialog).findViewById<EditText>(R.id.edit_text)
            try
            {
                val filename = files!!.pathToFiles + "/" + editText?.text.toString()
                files?.createFile(filename)
                tracker!!.start(File(filename))
                grabando = true;
                Toast.makeText(this, R.string.grabando, Toast.LENGTH_LONG).show()
            }
            catch(e: UnsupportedOperationException)
            {
                Toast.makeText(this, R.string.no_permission, Toast.LENGTH_LONG).show()
            }
            catch(e: IllegalArgumentException)
            {
                Toast.makeText(this, R.string.file_exists, Toast.LENGTH_LONG).show()
            }
        })
        builder?.setNegativeButton("Cancelar", DialogInterface.OnClickListener { dialog, which ->  })
        val dialog: AlertDialog? = builder?.create()
        dialog?.show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.preferencias) {
            val intent = Intent(this@MainActivity, PreferenceActivity::class.java)
            startActivityForResult(intent, 1)
        }
        return true
    }
}
