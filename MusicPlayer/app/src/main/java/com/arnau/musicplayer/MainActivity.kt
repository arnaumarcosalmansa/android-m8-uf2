package com.arnau.musicplayer

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.preference.PreferenceManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.EditText
import android.widget.ToggleButton
import com.arnau.musicplayer.dataobjects.PlayList
import com.arnau.musicplayer.dataobjects.Song

class MainActivity : BaseActivity() {

    var recyclerView: RecyclerView? = null

    var songs = mutableListOf<Song>()
    var playLists = mutableListOf<PlayList>()

    val songClickListener = object : SongListAdapter.ItemClickListener {
        override fun onItemClick(view: View, position: Int) {
            val songToStart = songs[position]

            val intent = Intent(applicationContext, PlaySongActivity::class.java)
            intent.putExtra("playlist", "_all")
            intent.putExtra("index", position)

            startActivityForResult(intent, PlaySongActivity.requestCode)
        }
    }

    val playListClickListener = object : PlayListListAdapter.ItemClickListener {
        override fun onItemClick(view: View, position: Int) {
            val playlist = playLists[position]

            val intent = Intent(applicationContext, PlayListActivity::class.java)
            intent.putExtra("name", playlist.name)
            startActivityForResult(intent, PlayListActivity.requestCode)
        }
    }

    val onLongClickListener = object : PlayListListAdapter.ItemClickListener {
        override fun onItemClick(view: View, position: Int) {
            val playlist = playLists[position]

            val intent = Intent(applicationContext, CreatePlaylistActivity::class.java)
            intent.putExtra("playlist_name", playlist.name)
            startActivityForResult(intent, CreatePlaylistActivity.requestCode)
        }
    }

    var buttonSongs: ToggleButton? = null
    var buttonPlaylists: ToggleButton? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.recyclerView = this.findViewById(R.id.list) as RecyclerView
        this.buttonSongs = findViewById<ToggleButton>(R.id.toggle_songs)
        this.buttonPlaylists = findViewById<ToggleButton>(R.id.toggle_lists)



        val supportActionBar = supportActionBar
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setTitle("Music Player")

        buttonSongs!!.toggle()
        buttonSongs!!.setOnClickListener(object : SoundOnClickListener {
            override fun onClick(v: View?) {
                super.onClick(v)
                if(buttonSongs!!.isChecked) {
                    buttonPlaylists?.toggle()

                    displaySongs()
                } else {
                    buttonSongs!!.toggle()
                }
            }
        })

        buttonPlaylists!!.setOnClickListener(object : SoundOnClickListener {
            override fun onClick(v: View?) {
                super.onClick(v)
                if(buttonPlaylists!!.isChecked) {
                    buttonSongs?.toggle()

                    displayPlayLists()
                } else {
                    buttonPlaylists!!.toggle()
                }
            }
        })

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
            &&
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
        ) {
            setup()
            Log.d("debug", "setup acabado")
        }
        else
        {
            //al recibir permisos ejecutaremos el setup
            askPermission()
        }
    }

    private fun setup()
    {
        val mediaMounted = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())

        if(mediaMounted) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(this)
            val path = prefs.getString("preference_path_to_songs", "/sdcard")
            if(prefs.getBoolean("flag_first_start", true)) {
                val allSongs = Song.findAll(this, path)
                val allPlaylist = PlayList("_all")
                allPlaylist.songs = allSongs.toMutableList()
                PlayList.save(this, allPlaylist)
                songs = allPlaylist.songs

                val edit = prefs.edit()
                edit.putBoolean("flag_first_start", false)
                edit.commit()
            } else {
                songs = PlayList.load(this, "_all").songs
            }

            playLists = PlayList.loadAll(this)

            displaySongs()

            val shouldGoToSong = prefs.getBoolean("preference_remember_song", false)
            val songPath = prefs.getString("preference_last_song", "")
            if (shouldGoToSong) {
                if (!songPath.equals("")) {
                    val splitted = songPath.split("/")

                    val intent = Intent(this, PlaySongActivity::class.java)

                    intent.putExtra("playlist", splitted[0])
                    intent.putExtra("index", splitted[1].toInt())
                    intent.putExtra("time", splitted[2].toInt())

                    startActivityForResult(intent, PlaySongActivity.requestCode)
                }
            }
        } else {
            dialogMediaNotMounted()
        }
    }

    private fun reload()
    {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val path = prefs.getString("preference_path_to_songs", "/sdcard")
        val allSongs = Song.findAll(this, path)

        val allPlaylist = PlayList("_all")
        allPlaylist.songs = allSongs.toMutableList()
        PlayList.save(this, allPlaylist)

        songs = allPlaylist.songs

        displaySongs()
    }

    private fun displaySongs()
    {
        this.recyclerView!!

        this.recyclerView?.adapter = null

        this.recyclerView?.layoutManager = null

        this.recyclerView?.layoutManager = LinearLayoutManager(this)

        this.recyclerView?.adapter = SongListAdapter(this, songs, songClickListener)
    }

    private fun displayPlayLists()
    {
        this.recyclerView!!

        this.recyclerView?.adapter = null

        this.recyclerView?.layoutManager = null

        this.recyclerView?.layoutManager = LinearLayoutManager(this)

        this.recyclerView?.adapter = PlayListListAdapter(this, playLists, playListClickListener, onLongClickListener)
    }

    //private fun toggle

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.preferencias) {
            val intent = Intent(this@MainActivity, PreferencesActivity::class.java)
            startActivityForResult(intent, PreferencesActivity.requestCode)
        } else if (item.itemId == R.id.add_playlist) {
            createNewPlaylistDialog()
        } else if (item.itemId == R.id.reload) {
            reload()
        }
        return true
    }

    fun askPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), 100);
    }


    fun createNewPlaylistDialog() {
        val builder: AlertDialog.Builder? = this?.let {
            AlertDialog.Builder(this)
        }
        builder?.setView(R.layout.modal_playlist_name)
        builder?.setTitle(R.string.playlist_name)
        builder?.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
            val nameText = (dialog as AlertDialog).findViewById<EditText>(R.id.text)
            val name = nameText!!.text.toString()
            if(!"".equals(name)) {
                val playlist = PlayList(name)
                PlayList.save(this, playlist)

                val intent = Intent(this, CreatePlaylistActivity::class.java)
                intent.putExtra("playlist_name", name)

                startActivityForResult(intent, CreatePlaylistActivity.requestCode)
            }
        })
        builder?.setNegativeButton(R.string.cancel, DialogInterface.OnClickListener { dialog, which ->  super.onBackPressed()})
        val dialog: AlertDialog? = builder?.create()
        dialog?.show()
    }

    fun dialogMediaNotMounted() {
        val builder: AlertDialog.Builder? = this?.let {
            AlertDialog.Builder(this)
        }
        builder?.setView(R.layout.modal_no_media_mounted)
        builder?.setTitle(R.string.error)
        builder?.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which -> })
        val dialog: AlertDialog? = builder?.create()
        dialog?.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == CreatePlaylistActivity.requestCode) {
            if(resultCode == Activity.RESULT_CANCELED) {
                playLists = PlayList.loadAll(this)
                displayPlayLists()
            }
        } else if(requestCode == PreferencesActivity.requestCode) {
            super.loadPreferences()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if(requestCode == 100) {
            setup()
        }
    }
}
