package com.arnau.musicplayer

import android.content.Context
import android.content.Intent
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.arnau.musicplayer.dataobjects.Song

class SongCheckboxListAdapter(val ctx: Context, val songs: List<Song>, val onClickListener: SongCheckboxListAdapter.ItemClickListener) : RecyclerView.Adapter<SongCheckboxListAdapter.SongViewHolder>()
{
    class ViewHolder(val view: LinearLayoutCompat) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongCheckboxListAdapter.SongViewHolder
    {
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.song_checkbox_list_item, parent, false) as LinearLayoutCompat
        return SongViewHolder(layout, onClickListener)
    }

    override fun onBindViewHolder(holder: SongViewHolder, position: Int)
    {
        val titleTextView = holder.view.findViewById<TextView>(R.id.title_text_view) as TextView
        val authorTextView = holder.view.findViewById<TextView>(R.id.author_text_view) as TextView
        val checkBox = holder.view.findViewById<CheckBox>(R.id.checkbox)

        val song = songs[position]

        titleTextView.text = song.name
        authorTextView.text = song.author
        checkBox.isChecked = song.selected
    }

    override fun getItemCount(): Int
    {
        return songs.size
    }

    interface ItemClickListener
    {
        fun onItemClick(view: View, position: Int)
    }

    class SongViewHolder(val view: View, val onClickListener: SongCheckboxListAdapter.ItemClickListener) : RecyclerView.ViewHolder(view), SoundOnClickListener {
        init
        {
            view.findViewById<CheckBox>(R.id.checkbox).setOnClickListener(this)
        }

        override fun onClick(v: View?)
        {
            super.onClick(v)
            onClickListener.onItemClick(v!!, adapterPosition)
        }
    }
}

