package com.arnau.musicplayer

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import java.util.*

open class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        BaseActivity.context = this

        loadPreferences()

        val supportActionBar = supportActionBar
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onResume() {
        super.onResume()
        loadPreferences()
    }

    protected fun loadPreferences() {
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val lang = preferences.getString("preference_language", "es")
        //val color = preferences.getString("preference_color", resources.getString(R.string.color_ttb))

        setLang(lang)
        //setColor(color)
    }

    protected fun setLang(lang: String?) {
        val res = this.resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = Locale(lang!!.toLowerCase())
        res.updateConfiguration(conf, dm)
    }

    protected fun setColor(color: String?) {
        val ab = supportActionBar
        ab!!.setBackgroundDrawable(ColorDrawable(Color.parseColor(color)))
        ab.setDisplayShowTitleEnabled(false)
        ab.setDisplayShowTitleEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return true
    }

    override fun onPause() {
        super.onPause()

        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val edit = prefs.edit()

        edit.putString("preference_last_song", "")

        edit.apply()
    }

    companion object {
        var context: Context? = null
    }
}
