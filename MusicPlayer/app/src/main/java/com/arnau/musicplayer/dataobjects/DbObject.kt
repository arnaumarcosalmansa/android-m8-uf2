package com.arnau.musicplayer.dataobjects

open class DbObject
{
    var id: Int = 0;

    fun isNew(): Boolean
    {
        return id == 0
    }
}