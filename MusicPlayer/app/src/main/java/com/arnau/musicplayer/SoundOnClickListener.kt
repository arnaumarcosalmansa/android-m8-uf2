package com.arnau.musicplayer

import android.media.AudioManager
import android.media.SoundPool
import android.support.v7.preference.PreferenceManager
import android.util.Log
import android.view.View

interface SoundOnClickListener : View.OnClickListener {
    override fun onClick(v: View?)
    {
        if (playSound) {
            SoundOnClickListener.soundPool.play(SoundOnClickListener.soundId!!, leftvol, rightvol, 1, 0, 1.0f)
            Log.d("sound", "click")
        }
    }

    companion object {
        val rightvol = 1.0f
        val leftvol = 1.0f

        var playSound: Boolean = false

        val soundPool = SoundPool(16, AudioManager.STREAM_MUSIC, 100)
        var soundId: Int? = null
        init {
            soundId = soundPool.load(BaseActivity.context, R.raw.effect_tick, 1);
            reload()
        }

        public fun reload()
        {
            val prefs = PreferenceManager.getDefaultSharedPreferences(BaseActivity.context)
            playSound = prefs.getBoolean("preference_play_sound_onclick", false)

            Log.d("click sound", playSound.toString())
        }
    }
}