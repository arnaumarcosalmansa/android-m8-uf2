package com.arnau.musicplayer

import android.media.MediaPlayer
import android.os.Bundle
import android.view.View
import com.arnau.musicplayer.dataobjects.PlayList
import android.media.AudioAttributes
import android.media.AudioManager
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.preference.PreferenceManager
import android.util.Log
import android.view.Menu
import android.widget.*
import kotlinx.android.synthetic.main.play_song_activity.*
import java.io.File
import java.util.concurrent.Semaphore


class PlaySongActivity : BaseActivity()
{
    val mHandler: Handler = Handler()

    var playing = true;

    var playList: PlayList? = null
    var index = 0

    var seekBar: SeekBar? = null
    var textMaxTime: TextView? = null
    var textCurrentTime: TextView? = null

    var textSongname: TextView? = null

    val seekBarUpdater = object : Thread() {
        override fun run() {
            super.run()
            mpSemaphore.acquire()
            seekBar!!.progress = mplayer?.currentPosition ?: 0
            val totalDuration = mplayer?.getDuration() ?: 0
            mpSemaphore.release()
            var currentPosition = 0
            while (currentPosition < totalDuration && !isInterrupted) {
                try {
                    Thread.sleep(500)
                    mpSemaphore.acquire()
                    currentPosition = mplayer!!.getCurrentPosition()
                    mpSemaphore.release()
                    seekBar!!.setProgress(currentPosition)
                    textCurrentTime?.text = intToTimeFormat(currentPosition)
                    currentPosition = 0
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                } catch(e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.play_song_activity)

        val playlistname = intent.getStringExtra("playlist")
        index = intent.getIntExtra("index", 0)
        val startTime = intent.getIntExtra("time", 0)

        playList = PlayList.load(this, playlistname)
        mpSemaphore.acquire()
        if (mplayer != null) {
            try {
                mplayer!!.apply{
                    reset()
                    prepare()
                    stop()
                    release()
                }
                mplayer = null
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        mplayer = MediaPlayer.create(applicationContext, Uri.fromFile(File(playList!!.songs[index].path)))

        mplayer!!.setOnCompletionListener( object : MediaPlayer.OnCompletionListener {
            override fun onCompletion(mp: MediaPlayer?) {
                moveSong(1)
            }
        })

        mpSemaphore.release()
        val playButton = findViewById<ImageButton>(R.id.button_play)

        playButton.setOnClickListener(object : SoundOnClickListener {
            override fun onClick(v: View?) {
                super.onClick(v)
                mpSemaphore.acquire()
                if (mplayer!!.isPlaying) {
                    mplayer?.pause()
                    playButton.setImageResource(android.R.drawable.ic_media_play)
                } else {
                    mplayer?.start()
                    playButton.setImageResource(android.R.drawable.ic_media_pause)
                }
                mpSemaphore.release()
            }
        })

        val nextButton = findViewById<ImageButton>(R.id.button_next)
        nextButton.setOnClickListener(object : SoundOnClickListener {
            override fun onClick(v: View?) {
                super.onClick(v)
                moveSong(1)
            }
        })

        val prevButton = findViewById<ImageButton>(R.id.button_prev)
        prevButton.setOnClickListener(object : SoundOnClickListener {
            override fun onClick(v: View?) {
                super.onClick(v)
                moveSong(-1)
            }
        })

        button_ff.setOnClickListener(object : SoundOnClickListener {
            override fun onClick(v: View?) {
                super.onClick(v)
                mplayer?.seekTo(mplayer!!.currentPosition + 5000)
            }
        })

        button_rew.setOnClickListener(object : SoundOnClickListener {
            override fun onClick(v: View?) {
                super.onClick(v)
                mplayer?.seekTo(mplayer!!.currentPosition - 5000)
            }
        })

        textMaxTime = findViewById(R.id.text_max_time)
        textCurrentTime = findViewById(R.id.text_current_time)
        seekBar = findViewById(R.id.bar_time)
        textSongname = findViewById(R.id.text_songname)


        seekBar!!.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            internal var seeked_progess: Int = 0
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                seeked_progess = progress
                if (fromUser) {
                    val mRunnable = object : Runnable {

                        override fun run() {
                            val min: Int
                            val sec: Int
                            mpSemaphore.acquire()
                            if (mplayer != null) {
                                val mCurrentPosition = seekBar.getProgress()
                                min = mCurrentPosition / 60
                                sec = mCurrentPosition % 60
                            }
                            mpSemaphore.release()
                            mHandler.postDelayed(this, 1000)
                        }
                    }
                    mRunnable.run()
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                mpSemaphore.acquire()
                mplayer?.seekTo(seekBar.progress)
                mpSemaphore.release()
            }
        })


        moveSong(0, startTime)

        seekBarUpdater.start()
    }

    fun moveSong(delta: Int, start: Int = 0)
    {
        index += delta

        index = if(index < 0) index + playList!!.songs.size else index
        index = if(index >= playList!!.songs.size) index - playList!!.songs.size else index

        textSongname!!.text = playList!!.songs[index].name

        button_play.setImageResource(android.R.drawable.ic_media_pause)

        mpSemaphore.acquire()
        if(delta != 0) {
            mplayer?.apply {
                reset()
                stop()
                release()
            }
            var fileSource = File(playList!!.songs[index].path)

            var problem = false;
            while(!fileSource.exists() && playList!!.songs.size > 0)
            {
                playList!!.songs.removeAt(index)

                index = if (index < playList!!.songs.size) index else playList!!.songs.size - 1
                fileSource = File(playList!!.songs[index].path)

                problem = true
            }

            if (problem) {
                PlayList.save(this, playList!!)
            }

            mplayer = MediaPlayer.create(applicationContext, Uri.fromFile(fileSource))

            mplayer!!.setOnCompletionListener( object : MediaPlayer.OnCompletionListener {
                override fun onCompletion(mp: MediaPlayer?) {
                    moveSong(1)
                }
            })
        }

        mplayer?.apply {
            stop()
            setAudioStreamType(AudioManager.STREAM_MUSIC)
            //setDataSource(playList!!.songs[index].path)
            prepare()

            seekBar!!.max = duration
            textMaxTime!!.text = intToTimeFormat(duration)

            start()

            seekTo(start)
        }
        mpSemaphore.release()
    }

    override fun onPause() {
        super.onPause()
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val edit = prefs.edit()
        mpSemaphore.acquire()
        edit.putString("preference_last_song", "" + playList!!.name + "/" + index + "/" + mplayer?.currentPosition)
        mpSemaphore.release()
        edit.apply()

        seekBarUpdater.interrupt()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        actionBar?.setIcon(R.mipmap.ic_launcher_my)
        supportActionBar?.setIcon(R.mipmap.ic_launcher_my)

        menuInflater.inflate(R.menu.icon, menu)

        return super.onCreateOptionsMenu(menu)
    }


    companion object {

        var mplayer: MediaPlayer? = null
        val mpSemaphore = Semaphore(1, true)
        val requestCode = 2

        fun intToTimeFormat(int: Int): String {

            var res = int / 1000
            val sec = res % 60
            res = res / 60
            val min = res % 60
            res = res / 60
            val hour = res / 60

            return hour.toString() + ":" + (if(min.toString().length < 2)  "0" else "") + min.toString() + ":" + (if(sec.toString().length < 2) "0" else "") + sec.toString()
        }
    }
}