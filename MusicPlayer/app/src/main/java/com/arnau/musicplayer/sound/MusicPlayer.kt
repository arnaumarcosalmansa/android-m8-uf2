package com.arnau.musicplayer.sound

import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import com.arnau.musicplayer.dataobjects.Song
import java.io.File
import java.security.AccessControlContext

class MusicPlayer(val context: Context) {
    public fun play(song: Song)
    {
        val uri = Uri.parse(song.path)
        val player = MediaPlayer.create(context, uri)
        player.prepare()
        player.start()
    }
}