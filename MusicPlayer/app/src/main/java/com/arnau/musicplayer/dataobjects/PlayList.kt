package com.arnau.musicplayer.dataobjects

import android.content.ContextWrapper
import android.support.v7.app.AppCompatActivity
import android.util.Log
import java.io.*

class PlayList(var name: String) : DbObject()
{
    var path: String? = null
    var songs: MutableList<Song> = mutableListOf()

    companion object {

        fun loadAll(activity: AppCompatActivity) : MutableList<PlayList> {
            val playListList = mutableListOf<PlayList>()

            val filesDir = File(activity.filesDir.absolutePath + "/playlists")
            filesDir.mkdir()

            val rootPath = filesDir.absolutePath
            val fileNames = filesDir.list()

            val paths = mutableListOf<String>()

            fileNames.forEach {
                val list = PlayList(it)
                playListList.add(list)

                val fr = FileReader(rootPath + "/" + it)
                val br = BufferedReader(fr)
                while (br.ready()) {
                    val line = br.readLine()

                    paths.add(line)
                }

                br.close()
                fr.close()

                val songs = Song.stringListToSongList(activity, paths)
                list.songs = songs.toMutableList()
            }

            return playListList
        }

        fun save(activity: AppCompatActivity, playList: PlayList) {
            val file = File(activity.filesDir.absolutePath + "/playlists/" + playList.name)
            file.parentFile.mkdirs()
            val fw = FileWriter(activity.filesDir.absolutePath + "/playlists/" + playList.name)
            playList.songs.forEach {
                fw.write(it.path)
                fw.write("\n")
                fw.flush()
            }
            fw.close()
        }

        fun load(activity: AppCompatActivity, name: String) : PlayList {
            val playlist = PlayList(name)

            val filesDir = File(activity.filesDir.absolutePath + "/playlists")
            filesDir.mkdir()

            val rootPath = filesDir.absolutePath
            val file = File(rootPath + "/" + name)

            val fr = FileReader(file)
            val br = BufferedReader(fr)

            val paths = mutableListOf<String>()

            while (br.ready()) {
                val line = br.readLine()
                paths.add(line)
            }

            val songs = Song.stringListToSongList(activity, paths).toMutableList()
            playlist.songs = songs

            return playlist
        }

        fun exists(activity: AppCompatActivity, name: String): Boolean {
            val filesDir = File(activity.filesDir.absolutePath + "/playlists/" + name)

            return filesDir.exists()
        }

        fun delete(activity: AppCompatActivity, name: String) {
            val filesDir = File(activity.filesDir.absolutePath + "/playlists/" + name)
            filesDir.delete()
        }
    }
}