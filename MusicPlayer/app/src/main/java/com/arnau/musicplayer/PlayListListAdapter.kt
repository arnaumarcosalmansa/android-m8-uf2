package com.arnau.musicplayer

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.arnau.musicplayer.dataobjects.PlayList
import com.arnau.musicplayer.dataobjects.Song


class PlayListListAdapter(val ctx: Context, val songs: MutableList<PlayList>, val onClickListener: ItemClickListener, val onLongClickListener: ItemClickListener) : RecyclerView.Adapter<PlayListListAdapter.PlayListViewHolder>()
{
    class ViewHolder(val view: LinearLayoutCompat) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayListViewHolder
    {
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.playlist_list_item, parent, false) as LinearLayoutCompat
        return PlayListViewHolder(layout, onClickListener, onLongClickListener)
    }

    override fun onBindViewHolder(holder: PlayListViewHolder, position: Int)
    {
        val titleTextView = holder.view.findViewById<TextView>(R.id.title_text_view) as TextView
        val iconImageView = holder.view.findViewById<ImageView>(R.id.icon_image_view) as ImageView

        val song = songs[position]

        titleTextView.text = song.name
        iconImageView.setImageResource(R.drawable.ic_list)
    }

    override fun getItemCount(): Int
    {
        return songs.size
    }

    interface ItemClickListener {
        fun onItemClick(view: View, position: Int)
    }

    class PlayListViewHolder(val view: View, val onClickListener: PlayListListAdapter.ItemClickListener, val onLongClickListener: ItemClickListener) : RecyclerView.ViewHolder(view), SoundOnClickListener, View.OnLongClickListener {

        init
        {
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
        }

        override fun onClick(v: View?)
        {
            super.onClick(v)
            onClickListener.onItemClick(v!!, adapterPosition)
        }

        override fun onLongClick(v: View?): Boolean
        {
            onLongClickListener.onItemClick(v!!, adapterPosition)
            return true
        }
    }
}