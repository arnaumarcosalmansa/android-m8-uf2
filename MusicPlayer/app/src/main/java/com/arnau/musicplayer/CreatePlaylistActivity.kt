package com.arnau.musicplayer

import android.app.Activity
import android.content.DialogInterface
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import com.arnau.musicplayer.dataobjects.PlayList
import kotlinx.android.synthetic.main.create_playlist_activity.*

class CreatePlaylistActivity : BaseActivity() {

    var playList: PlayList? = null

    var allPlaylist: PlayList? = null

    var checkeds: Array<Boolean>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.create_playlist_activity)

        val playlistName = intent.getStringExtra("playlist_name")
        if(PlayList.exists(this, playlistName)) {
            playList = PlayList.load(this, playlistName)
        } else {
            playList = PlayList(playlistName)
        }
        allPlaylist = PlayList.load(this, "_all")

        checkeds = Array(allPlaylist!!.songs.size, { i -> false })

        for(supersong in allPlaylist!!.songs) {
            for(subsong in playList!!.songs) {
                if(supersong.path.equals(subsong.path)) {
                    supersong.selected = true
                }
            }
        }

        val list = findViewById<RecyclerView>(R.id.list)
        list.layoutManager = LinearLayoutManager(this)
        list.adapter = SongCheckboxListAdapter(this, allPlaylist!!.songs, object : SongCheckboxListAdapter.ItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                allPlaylist!!.songs[position].selected = (view as CheckBox).isChecked
            }
        })

        save_button.setOnClickListener(object : SoundOnClickListener {
            override fun onClick(v: View?) {
                super.onClick(v)
                playList!!.songs.clear()

                allPlaylist!!.songs.forEach {

                    if(it.selected) {
                        playList!!.songs.add(it)
                    }
                }

                PlayList.save(this@CreatePlaylistActivity, playList!!)
            }
        })

        delete_button.setOnClickListener(object : SoundOnClickListener {
            override fun onClick(v: View?) {
                super.onClick(v)
                PlayList.delete(this@CreatePlaylistActivity, playList!!.name)
                setResult(Activity.RESULT_CANCELED)
                finish()
            }
        })
    }

    companion object {
        val requestCode = 5
    }
}