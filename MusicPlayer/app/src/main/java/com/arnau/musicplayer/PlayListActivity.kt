package com.arnau.musicplayer

import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.AdapterView
import com.arnau.musicplayer.dataobjects.PlayList
import com.arnau.musicplayer.dataobjects.Song

class PlayListActivity : BaseActivity(){

    var recyclerView: RecyclerView? = null

    var playlist: PlayList? = null
    var songs = mutableListOf<Song>()

    val songClickListener = object : SongListAdapter.ItemClickListener {
        override fun onItemClick(view: View, position: Int) {
            val songToStart = songs[position]

            val intent = Intent(applicationContext, PlaySongActivity::class.java)
            intent.putExtra("playlist", playlist!!.name)
            intent.putExtra("index", position)

            startActivityForResult(intent, PlaySongActivity.requestCode)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.playlist_activity)

        recyclerView = findViewById<RecyclerView>(R.id.list)

        val playlistName = intent.getStringExtra("name")
        playlist = PlayList.load(this, playlistName)
        songs = playlist!!.songs
        displaySongs()
    }

    private fun displaySongs()
    {
        this.recyclerView!!

        this.recyclerView?.adapter = null

        this.recyclerView?.layoutManager = LinearLayoutManager(this)

        this.recyclerView?.adapter = SongListAdapter(this, songs, songClickListener)
    }

    companion object {
        val requestCode = 3
    }
}