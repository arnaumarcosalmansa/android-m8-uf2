package com.arnau.musicplayer.dataobjects

import android.media.MediaMetadataRetriever
import android.support.v7.app.AppCompatActivity
import android.util.Log
import java.io.File

class Song(val path: String, val name: String, val author: String) : DbObject()
{
    var selected = false

    companion object {
        fun allSongs() {

        }

        fun findAll(activity: AppCompatActivity, rootPath: String) : List<Song> {
            //val dir = activity.getExternalFilesDir(null)
            val dir = File(rootPath)
            Log.d("file", "external files dir : " + dir.absolutePath)

            val allSongs = mutableListOf<String>()

            findMusicInFolder(dir, allSongs)

            return stringListToSongList(activity, allSongs)
        }

        fun stringListToSongList(activity: AppCompatActivity, stringList: List<String>) : List<Song> {
            val  metaRetriever = MediaMetadataRetriever()
            return stringList.map {
                metaRetriever.setDataSource(it)
                val artist =  metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST) ?: "Unknown artist";
                val title = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE) ?: "Unknown title";
                Song(it, title, artist)
            }.toList()
        }

        private fun findMusicInFolder(file: File, songList: MutableList<String>) {
            val content = file.list()
            for (path in content) {
                val sub = File(file.absolutePath + "/" + path)
                if (sub.isDirectory) {
                    findMusicInFolder(sub, songList)
                } else {
                    val filename = sub.name
                    if (filename.endsWith(".mp3") || filename.endsWith(".wav")) {
                        songList.add(sub.absolutePath)
                    }
                }
            }
        }
    }
}