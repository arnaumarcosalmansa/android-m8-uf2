package com.arnau.musicplayer

import android.content.Context
import android.content.Intent
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.arnau.musicplayer.dataobjects.Song

class SongListAdapter(val ctx: Context, val songs: List<Song>, val onClickListener: SongListAdapter.ItemClickListener) : RecyclerView.Adapter<SongListAdapter.SongViewHolder>()
{
    class ViewHolder(val view: LinearLayoutCompat) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongListAdapter.SongViewHolder
    {
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.song_list_item, parent, false) as LinearLayoutCompat
        return SongViewHolder(layout, onClickListener)
    }

    override fun onBindViewHolder(holder: SongViewHolder, position: Int)
    {
        val titleTextView = holder.view.findViewById<TextView>(R.id.title_text_view) as TextView
        val iconImageView = holder.view.findViewById<ImageView>(R.id.icon_image_view) as ImageView
        val authorTextView = holder.view.findViewById<TextView>(R.id.author_text_view) as TextView

        val song = songs[position]

        titleTextView.text = song.name
        authorTextView.text = song.author
        iconImageView.setImageResource(R.drawable.list_item_icon)
    }

    override fun getItemCount(): Int
    {
        return songs.size
    }

    interface ItemClickListener
    {
        fun onItemClick(view: View, position: Int)
    }

    class SongViewHolder(val view: View, val onClickListener: SongListAdapter.ItemClickListener) : RecyclerView.ViewHolder(view), SoundOnClickListener {
        init
        {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?)
        {
            super.onClick(v)
            onClickListener.onItemClick(v!!, adapterPosition)
        }
    }
}

