package com.earnau.videoplayer;

import java.io.IOException;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.view.Menu;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements SurfaceHolder.Callback{

	private boolean recording = false; // Indica si se est� grabando
	private boolean playing = false; // Indica si el v�deo est� reproduci�ndose
	private MediaRecorder mediaRecorder; // Permite la grabaci�n
	private MediaPlayer mediaPlayer; // Permite la reproducci�n
	private String FileName; // Ruta de almacenamiento del archivo

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		

		FileName = Environment.getExternalStorageDirectory() + "/myvideo.mp4";

		SurfaceView surface = (SurfaceView) findViewById(R.id.surfaceView);
		/* No se puede manejar directamente el objeto Surface, 
		 * se debe hacer a trav�s de un SurfaceHolder (un contenedor) que se consigue 
		 * llamando en el constructor de la clase a getHolder() e indicar que 
		 * SurfaceHolder va a recibir las llamadas del SurfaceHolder.callback: */
		
		surface.getHolder().addCallback(this);
		surface.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		final Button recordButton = (Button) findViewById(R.id.btnGrabar);
		final Button stopButton = (Button) findViewById(R.id.btnParar);
		final Button playButton = (Button) findViewById(R.id.btnRepr);

	      // Evento al pulsar el bot�n de grabaci�n.
	    recordButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				recordButton.setEnabled(false);
				stopButton.setEnabled(true);
				playButton.setEnabled(false);
				recording = true;
	
				// Se establecen las opciones de audio y de v�deo para la grabaci�n
		    	mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		    	mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
		    	mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
		    	mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
		    	mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.MPEG_4_SP);
	
		    	// Ruta de grabaci�n
		    	mediaRecorder.setOutputFile(FileName); // path);
	
		    	try {
					mediaRecorder.prepare();
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	
				mediaRecorder.start();
			}
	    });  // ecordButton.setOnClickListener

		

	    
	      // Evento del bot�n de detenc�n de la grabaci�n o reproducci�n
        stopButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				recordButton.setEnabled(true);
				stopButton.setEnabled(false);
				playButton.setEnabled(true);
	
				if (recording){
					mediaRecorder.stop();
					mediaRecorder.reset();
					recording = false;
				}else{
					mediaPlayer.stop();
					mediaPlayer.reset();
					playButton.setText("Play");
				}
			}
		});  //         stopButton.setOnClickListener
        
        

        // Evento del bot�n de play
        playButton.setOnClickListener(new OnClickListener() {

		  	public void onClick(View v) {
		  		recordButton.setEnabled(false);
		  		stopButton.setEnabled(true);
		  		playButton.setEnabled(true);
		
		  		// Si no se est� reproducciendo, estaba parado o pausado
		  		if (!mediaPlayer.isPlaying()){
		
		  			playButton.setText("Pause");
		
		  			// Evento cuando se llega al final de la reproducci�n
		  			mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
		
		  				public void onCompletion(MediaPlayer mp) {
		  					recordButton.setEnabled(true);
		  					stopButton.setEnabled(false);
		  					playButton.setEnabled(true);
		  					playButton.setText("Play");
		  				}
		  			});
		
		  			// Si todav�a no se ha iniciado la reproducci�n (no estaba pausado), se prepara el reproductor
		  			if (mediaPlayer.getCurrentPosition() == 0){
		  				try{
		  					mediaPlayer.setDataSource(FileName);  //path);
		  					mediaPlayer.prepare();
		  				} catch(IllegalArgumentException e) {
		  				} catch (IOException e) {
		  				}
		  			}
		
		  			mediaPlayer.start();
		  		}else{
		  			// El bot�n de play tambi�n funciona para pausar cuando el v�deo se est� reproduciendo
		  			mediaPlayer.pause();
		  			playButton.setText("Play");
		  		}
		  	}
		  });  // playButton.setOnClickListener
		
	    
	    

	}

	
	
	
	
	
	
	
	
	
	
	/* De los tres m�todos que la interfaz nos pide implementar, 
	 * s�lo hay dos que necesitemos en este caso. 
	 * El surfaceCreated se lanza cuando se crea la surface, y lo que hace es inicializar los reproductores:
	 * El surfaceDestroyed se limita a liberar los recursos asociados a dichos reproductores:
	 * El surfaceChanged se produce cuando hay algun cambio en el dispositivo: por ejemplo cambio de orientacion del dispositivo
	 */

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {

		// Inicializaci�n del reproductor y grabador si son nulos
		if (mediaRecorder == null){
			mediaRecorder = new MediaRecorder();
			mediaRecorder.setPreviewDisplay(holder.getSurface());
		}
		if (mediaPlayer == null){
			mediaPlayer = new MediaPlayer();
			mediaPlayer.setDisplay(holder);
		}
	}

	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// Se liberan los recursos asociados con estos objetos
		mediaRecorder.release();
		mediaPlayer.release();
	}


}
