package com.arnau.galeriasd

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.support.v4.view.ViewPager
import android.util.Log
import java.io.FileOutputStream
import java.io.File


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        val storagePresent = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)
        if(storagePresent)
        {
            copyAssetsToFolder()
            val files = getFiles()
            val galeria = findViewById<ViewPager>(R.id.galeria)
            galeria.adapter = ImageAdapter(this, files);
        }
        else
        {
            throw Exception()
        }
    }

    fun copyAssetsToFolder()
    {
        val extStore = this.getExternalFilesDir(null)
        val assetNames = this.assets.list("myimages")
        assetNames.forEach {
            Log.d("file", it)

            val inputStream = this.assets.open("myimages/" + it)
            val file = File(extStore.absolutePath)
            file.mkdirs()
            val outputStream = FileOutputStream(extStore.absolutePath + "/" + it)

            while(true)
            {
                val read = inputStream.read()
                if (read < 0) break;
                outputStream.write(read)
            }

            outputStream.flush()
            outputStream.close()
            inputStream.close()

        }
    }

    fun getFiles(): ArrayList<File>
    {
        val extStore = this.getExternalFilesDir(null)
        val path = extStore.absolutePath
        val names = extStore.list()
        val files = ArrayList<File>(names.size)
        names.forEach {
            files.add(File(path + "/" + it))
        }

        return files
    }
}
