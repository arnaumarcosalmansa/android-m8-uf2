package com.arnau.galeriasd

import android.content.Context
import android.graphics.BitmapFactory
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.support.v4.view.ViewPager
import com.arnau.galeriasd.R
import java.io.File


class ImageAdapter(val ctx: Context, val files: ArrayList<File>) : PagerAdapter()
{
    val inflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return files.size
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj as LinearLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        //super.instantiateItem(container, position)
        val view = inflater.inflate(R.layout.image_page, container, false)
        val imageView = view.findViewById<ImageView>(R.id.image_view)

        imageView.setImageBitmap(BitmapFactory.decodeFile(files[position].absolutePath))
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as LinearLayout)
    }
}